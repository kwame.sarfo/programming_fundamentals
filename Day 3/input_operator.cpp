// Write a program that prompts for two arguments and an operator. Using a switch statement, evaluate the expression formed by the operator and operands.

// For example:

// Please input two operands:  2  3
// Please input an operator:  *
// Multiplying 2 and 3 = 6
 
// Please input two operands: 4 8
// Please input an operator: +
// Adding 4 and 8 = 12

#include <iostream>
#include <cstring>

int main()
{
    std::cout << "Please enter two numbers" << std::endl;
    int first_number, second_number {0};

    std::cout << "number 1: ";
    std::cin >> first_number;

    std::cout << "number 2: ";
    std::cin >> second_number;

    std::cout << "Please enter an operation: ";
    char operation;
    std::cin >> operation;

    std::string operation_name {};
    int result {};

    switch (operation)
    {
    case '+':
        operation_name = "Adding";
        result = first_number + second_number;
        break;
    case '-':
        operation_name = "Subtracting";
        result = first_number - second_number;
        break;
    case '*':
        operation_name = "Multiplying";
        result = first_number * second_number;
        break;
    case '/':
        operation_name = "Dividing";
        result = first_number / second_number;
        break;
    }

    std::cout << operation_name << " " << first_number << " and " << second_number << " = " << result;

    return EXIT_SUCCESS;
}