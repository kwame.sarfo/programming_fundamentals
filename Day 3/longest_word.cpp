// Write a program that reads a line of words from the input and prints out the longest word and its length. Make use of standard library types such as 
// std::string
// , 
// std::list
// , and 
// std::vector
// .


#include <iostream>
#include <cstdlib>
#include <cstring>
#include <vector>

int main()
{
    std::cout << "Please enter a string: ";

    std::vector<char> input {};

    char letter;

    while (std::cin.get(letter) && letter != '\n')
    {
        input.push_back(letter);
    }

    

    int counter {0};
    int max_length {0};
    std::string max_string {};

    for (int iter = 0; iter < input.size(); iter++)
    {
        if (input[iter +1] != char(32) && input[iter + 1])
        {   
            continue;
        };
       if ((iter-counter)+1 > max_length){
            max_length = iter-counter + 1;
            max_string = "";
            while (counter <= iter)
            {
                max_string += input[counter];
                counter++;
            }    
        } 
        counter = iter + 2;  
    }
    std::cout << "the longest word is " << max_string ;
    return EXIT_SUCCESS;
}


