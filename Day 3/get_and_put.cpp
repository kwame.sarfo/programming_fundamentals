// The following program fragment reads characters from standard input and writes them to the standard output until end-of-input is detected. Write a program to verify this behaviour. Note that end-of-input on Unix machines is indicated by ^D.

// char c;
// while (std::cin.get(c))
//   std::cout.put(c);

#include <iostream>

int main()
{
    char c;
    while (std::cin.get(c))
    {
        std::cout.put(c);
    }   
}