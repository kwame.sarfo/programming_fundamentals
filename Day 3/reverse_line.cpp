// Write a program which reads in a line of text, reverses all of the characters, and writes out the reversed line.

// In this exercise use the 
// cin.get()
//  function to ensure that the entire line is reversed, not just the first word.

#include <iostream>
#include <cstring>
#include <vector>

int main()
{
    std::vector<char> input {};
    char letter;
    std::cout << "Please enter a line" << std::endl;

    while (std::cin.get(letter) && letter != '\n')
    {
        input.push_back(letter);
    }

    for (int iter= input.size()-1; iter>=0; iter--){
        std::cout << input[iter];
    }

    return EXIT_SUCCESS;

}