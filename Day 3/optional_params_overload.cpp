// Replace the 
// std::string
//  overload above with one that takes a 
// char []
// . Overload the function to optionally take two or three parameters.

// output(myname)        
// // causes the complete string to be displayed on the terminal
 
// output(myname,3)
// // causes part of the string to be displayed starting from character 3
 
// output(myname,3,4)
// // causes a sub-string of myname to be displayed starting at offset 3 and of length 4. 
// // (A length of -1 may be used to indicate end of string).


#include <iostream>
#include <cstring>


void output(char c[9])
{
    std::cout << c << std::endl;
}

void output(char c[9], int string_offset)
{
    for (int iter = string_offset; iter < strlen(c); iter ++)
    {
        std::cout << c[iter];
    } 
    std::cout << std::endl;
}

void output(char c[9], int string_offset, int offset_length)
{
    for (int iter = string_offset; iter < string_offset + offset_length; iter ++)
    {
        std::cout << c[iter];
    }
    std::cout << std::endl;
}

int main()
{
    char string_chars[] {"hi there"};
    output(string_chars);
    output(string_chars, 3);
    output(string_chars, 3, 4);

    return EXIT_SUCCESS;
}