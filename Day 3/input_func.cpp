// Write a function called 
// input
//  that prompts for two integers and
// then passes these back to the caller. The calling function should simply add them together and output the result.

#include <iostream>

void input(int *first_number, int *second_number)
{
    std::cout << "Please input 2 numbers" << std::endl;

    std::cout << "Please input first number: ";
    std::cin >> *first_number;

    std::cout << "Please input second number: ";
    std::cin >> *second_number;

}

int main()
{
    int first_number, second_number {};
    input(&first_number, &second_number);

    std::cout << "Output is " << first_number + second_number ;

    return EXIT_SUCCESS;
}