// Write a collection of functions to output values of different types onto the display. All of the functions should have the same name, but be overloaded for int, 
// char
// , 
// float
// , 
// long
//  and 
// std::string
// . The functions should also output their type.

// Write a program to test these functions. Exercise the functions using both literal constants and variables.

// output(3.14)
// output("hi there");
// output(myname);
// Which function(s) does the call to 
// output(3.14
// ) attempt to bind to and why?


#include <iostream>

void output(float float_num)
{
    std::cout << float_num << std::endl;
}

void output(std::string string_value)
{
    std::cout << string_value << std::endl;
}

void output(int my_integer)
{
    std::cout << my_integer << std::endl;
}

void output(char c)
{
    std::cout << c << std::endl;
}

int main()
{
    output((float)3.14);
    output("hi there");
    output(123);
    output('c');

    return EXIT_SUCCESS;
}

//because in c++ decimals are defined as doubles and so a float decimal has to be explicitly stated