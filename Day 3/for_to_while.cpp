// Rewrite the following for statement as an equivalent while statement.

// for ( i = 0; i < max_length; ++i )
//   if ( input_line[i] == '?' ) ++count;

#include <iostream>
#include <cstdlib>
#include <cstring>


int main()
{
    char input_line[256];

    std::cout << "Please input a string: ";
    std::cin >> input_line;

    int iter {0};
    int count {0};

    while (iter < strlen(input_line))
    {
        if (input_line[iter] == '?')
        {
            ++count;
        }
        iter++;
    }
    std::cout << count;
    return EXIT_SUCCESS;
}