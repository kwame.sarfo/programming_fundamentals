/*Write a program in which you define variables of the following types

integer
pointer to integer
reference to integer
constant integer
Perform the following actions on the variables

assign the value 5 to the integer
increment the integer through the pointer
increment the integer through the reference
output the integer and verify that its value is 7
What happens if you attempt to change the value of the constant?

Output the decimal values of the following literals:

0xf3f2
0437
'a'
*/

#include <iostream>
#include <cstdlib>

int main() 
{
    int my_integer;
    int *my_integer_ptr = &my_integer;
    int &my_integer_reference = my_integer;
    const int const_integer{30};

    my_integer = 5;
    (*my_integer_ptr)++;
    my_integer_reference++;


    std::cout << "actual int " << my_integer << std::endl;
    std::cout << "actual integer reference " << &my_integer << std::endl;
    std::cout << "my_integer_reference " << &my_integer_reference << std::endl;
    std::cout << "my_integer_reference value " << my_integer_reference << std::endl;
    std::cout << "my_integer_ptr deref" << *my_integer_ptr << std::endl;
    std::cout << "my_integer_ptr " << my_integer_ptr << std::endl;
    std::cout << "my_integer_ptr ref" << &my_integer_ptr << std::endl;
    std::cout << "decimal " << (int)'a' << std::endl;
    return EXIT_SUCCESS;
}