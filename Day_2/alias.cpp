#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>

int main()
{
    using string_type = std::string [10];
    string_type string_arr;


    for (int iter = 0; iter < 10; iter++)
    {
        std::cout << "Please enter the " << (iter) +1 << " string: "; 
        std::cin >> string_arr[iter];
    }

    int max_length {0};
    std::string max_string {};

    for (int iter = 0; iter < 10; iter++)
    {
        if(string_arr[iter].length() > max_length)
        {
            max_length = string_arr[iter].length();
            max_string = string_arr[iter];
        }
    }
    std::cout << "The longest string is: " << max_string << std::endl;

    return EXIT_SUCCESS;

}