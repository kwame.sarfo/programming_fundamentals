/*Write a program which reads a single line of hyphen connected words and outputs the words one per line, with a count of the number of characters in each. Your solution should employ primitive C style strings, using character arrays and pointers.

Enter word: hello-this-is-a-line-of-input
[5] hello
[4] this
[2] is
[1] a
[4] line
[2] of
[5] input
Note: when using the input operator 
>>
 all characters are read until a white-space character is found.

Ensure that your solution to this and all subsequent questions is properly indented and formatted.
*/
// hello-this-is-a-line-of-input-
// 5       4   2  1  4   2   5

#include <iostream>
#include <cstdlib>
#include <cstring>

int main()
{
    std::cout << "Please enter a string: ";

    char input[256];

    std::cin >> input;

    int counter {0};

    for (int iter = 0; iter < strlen(input); iter++)
    {
        if (input[iter +1] != '-' && input[iter + 1])
        {   
            continue;
        };
        std::cout << "[" << (iter-counter) + 1 << "] ";
        while (counter <= iter)
        {
            std::cout << input[counter];
            counter++;
        }
        std::cout << std::endl;
        counter = iter + 2;   
    }


    return EXIT_SUCCESS;
}