// Given the following enumeration type, write a program to output the values associated with each symbol.

// enum {RED, YELLOW, AMBER=YELLOW, GREEN};

#include <iostream>
#include <cstdlib>

int main()
{
    enum {
        RED,
        YELLOW,
        AMBER=YELLOW,
        GREEN
    };

    std::cout << "RED: " << RED << std::endl;
    std::cout << "YELLOW: " << YELLOW << std::endl;
    std::cout << "AMBER: " << AMBER << std::endl;
    std::cout << "GREEN: " << GREEN << std::endl;

    return EXIT_SUCCESS;
}