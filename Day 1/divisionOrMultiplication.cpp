#include <iostream>
using namespace std;

int inputNumber()
{
    cout << "Please enter a number: ";
    int num;
    cin >> num;
    return num;
}

int main()
{
    // number from standard input
    int mainInputNumber = inputNumber();

    if(mainInputNumber % 2 == 0)
    {
        for(int iter = 1; iter <= 20; ++iter){
            cout << mainInputNumber << " x " << iter << " = " << mainInputNumber * iter << endl;
        }
    }
    else
    {
        for(int iter = 1; iter <= 30; ++iter)
        {
            if(iter % mainInputNumber != 0){
                cout << iter << endl;
            }
        }
    }
}